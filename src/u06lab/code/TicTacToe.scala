package u06lab.code

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  @scala.annotation.tailrec
  def find(board: Board, x: Int, y: Int): Option[Player] = board match {
    case h :: _ if h.x == x && h.y == y => Some(h.player)
    case _ :: t => find(t, x, y)
    case _ => None
  }

  val size: Int = 3
  def placeAnyMark(board: Board, player: Player): Seq[Board] = for (i <- 0 until size;
                                                                    j <- 0 until size
                                                                    if this.find(board, i, j).isEmpty)
                                                                    yield Mark(i, j, player) :: board

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
      case 0 => Stream(List(List()))
      case _ => for {
        game <- this.computeAnyGame(player.other, moves - 1)
        board <- this.placeAnyMark(game.head, player)
        g = if(!this.isEnd(game.head)) board :: game else game
      } yield g
  }

  def isEnd(board: Board): Boolean = {
    for (m1 <- board){
      for (m2 <- board){
        for (m3 <- board){
          if(m1.player == m2.player && m1.player == m3.player){
            if(m1.x == m2.x && m1.x == m3.x && m1.y != m2.y && m1.y != m3.y && m2.y != m3.y){
              return true
            }
            if(m1.y == m2.y && m1.y == m3.y && m1.x != m2.x && m1.x != m3.x && m2.x != m3.x){
              return true
            }
            if(m1.x == 0 && m1.y == m1.x && m2.x == 1 && m2.y == m2.x && m3.x == 2 && m3.y == m3.x) {
              return true
            }
            if(m1.x == 0 && m1.y == 2 && m2.x == 1 && m2.y == m2.x && m3.x == 2 && m3.y == 0) {
              return true
            }
          }
        }
      }
    }

    false
  }


  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  val stream = computeAnyGame(O, 9)
  println(stream.size)
  stream foreach {g => printBoards(g); println()}
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
}